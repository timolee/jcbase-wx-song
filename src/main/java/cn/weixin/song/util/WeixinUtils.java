package cn.weixin.song.util;

import java.io.File;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.weixin.song.model.App;

import com.github.sd4324530.fastweixin.api.CustomAPI;
import com.github.sd4324530.fastweixin.api.JsAPI;
import com.github.sd4324530.fastweixin.api.MaterialAPI;
import com.github.sd4324530.fastweixin.api.MenuAPI;
import com.github.sd4324530.fastweixin.api.OauthAPI;
import com.github.sd4324530.fastweixin.api.QrcodeAPI;
import com.github.sd4324530.fastweixin.api.TemplateMsgAPI;
import com.github.sd4324530.fastweixin.api.UserAPI;
import com.github.sd4324530.fastweixin.api.config.ApiConfig;
import com.github.sd4324530.fastweixin.api.response.UploadMaterialResponse;
import com.google.common.collect.Maps;
/**
 * 微信工具类
 */
public final class WeixinUtils {

	public final Log logger = LogFactory.getLog(getClass());
	
	public static final Map<String,ApiConfig> appMaps=Maps.newHashMap();
	/**
	 * 获取wid对应的ApiConfig对象
	 */
	public static ApiConfig getApiConfig(String wid){
		if(appMaps.containsKey(wid)){
			return appMaps.get(wid);
		}else{
			App app=App.dao.getApp(wid);
			if(app!=null){
				ApiConfig apiConfig=new ApiConfig(app.getWxAppId(),app.getAppsecret(),true);
				appMaps.put(wid, apiConfig);
				return apiConfig;
			}
		}
		return null;
	}
	public static QrcodeAPI getQrcodeAPI(String wid){
		return new QrcodeAPI(getApiConfig(wid));
	}
	/**
	 * 获取UserAPI
	 */
	public static UserAPI getUserAPI(String wid){
		return new UserAPI(getApiConfig(wid));
	}
	/**
	 * 获取MenuAPI
	 */
	public static MenuAPI getMenuAPI(String wid){
    	return new MenuAPI(getApiConfig(wid));
	}
	/**
	 * 获取CustomAPI
	 */
	public static CustomAPI getCustomAPI(String wid){
		return new CustomAPI(getApiConfig(wid));
	}
	/**
	 * 网页授权API
	 */
	public static OauthAPI getOauthAPI(String wid){
		return new OauthAPI(getApiConfig(wid));
	}

	public static JsAPI getJsAPI(String wid){
		return new JsAPI(getApiConfig(wid));
	}
	
	public static TemplateMsgAPI getTemplateMsgAPI(String wid){
		return new TemplateMsgAPI(getApiConfig(wid));
	}
	public static UploadMaterialResponse uploadThumb(File file,String wid){
		MaterialAPI materialAPI=new MaterialAPI(getApiConfig(wid));
		return  materialAPI.uploadMaterialFile(file);
	}
	
}

