function searcha() {
	$(".prer03 li p em").live("click",
	function() {
		$(this).parents("li").css("display", "none")
	})
}
function searcher(n) {
	$(n).bind({
		keyup: function() {
			$(this).val().length > 0 ? $(this).next("span").show() : $(this).next("span").hide()
		},
		blur: function() {
			setTimeout(function() {
				$(n).next("span").hide()
			},
			500)
		},
		focus: function() {
			$(this).val().length > 0 && $(this).next("span").show()
		}
	}),
	$(n).next("span").bind("click",
	function() {
		$(this).hide(),
		$(this).prev("input").val("")
	})
}
function checkEmail(n) {
	return matchEmail(n) ? !0 : (PopUp("请输入正确的Email地址"), !1)
}
function checkTelephone(n) {
	var t = /(^18\d{9}$)|(^13\d{9}$)|(^15\d{9}$)/;
	return t.test(n) ? !0 : (PopUp("请输入正确的手机号"), !1)
}
function checkEmailAndTelephone(n) {
	var t = /^(18\d{9}|13\d{9}|15\d{9}|\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)$/;
	return t.test(n) ? !0 : (PopUp("请输入正确的手机号或Email"), !1)
}
function checkPwd(n) {
	var t = /^[a-zA-Z0-9]{8,}$/;
	return t.test(n) ? !0 : (PopUp("密码不符合要求：a~z,0~9,区分大小写,最少8位数值."), !1)
}
function matchEmail(n) {
	var t = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	return t.test(n) ? !0 : !1
}
function PopUp(n, t) {
	var r, u, e, f;
	r = $('<div id="mask" style="display:none"></div>'),
	r.appendTo("body"),
	u = $('<div class="popup w580" style="display:none"><div class="popup-hd"><a id="closex" title="关闭" class="closex closegb" href="javascript:void(0);"><span>关闭</span></a></div><h3>提示</h3><p id="alertbox-msg" class="position02"></p><div class="bgPray"><input id="alertbox-OK" class="inputBtn05 closegb" type="button" value="确定"><div class="clear"></div></div></div>'),
	u.appendTo("body"),
	e = u.find("#alertbox-msg"),
	e.html(n),
	showBox(r, u),
	f = u.find("#alertbox-OK");
	f.on("click",
	function() {
		f.off(),
		closeBox(r, u),
		typeof t == "function" && t();
	});
	r.on("click",
	function() {
		f.off(),
		r.off(),
		closeBox(r, u);
	});;
}
function ConfirmBox(n, t, i) {
	var u, r, o, f, e;
	u = $('<div id="mask" style="display:none"></div>'),
	u.appendTo("body"),
	r = $('<div class="popup w580" style="display: none;"><div class="popup-hd"><a id="closex" title="关闭" class="closex closegb" href="javascript:void(0);"><span>关闭</span></a></div><h3>提示</h3><p id="alertbox-msg" class="position"></p><div class="bgPray popclear"><div class="w200"><input id="alertbox-Cancel" class="inputBtn03 closegb" type="button" value="取消"><input id="alertbox-OK" class="inputBtn04 closegb" type="button" value="确定"><div class="clear"></div></div></div></div>'),
	r.appendTo("body"),
	o = r.find("#alertbox-msg"),
	o.html(n),
	showBox(u, r),
	f = r.find("#alertbox-OK"),
	e = r.find("#alertbox-Cancel");
	f.on("click",
	function() {
		f.off(),
		e.off(),
		closeBox(u, r),
		typeof t == "function" && t();
	});
	$("#alertbox-Cancel").on("click",
	function() {
		f.off(),
		e.off(),
		closeBox(u, r),
		typeof i == "function" && i();
	});
	u.on("click",
	function() {
		f.off(),
		e.off(),
		u.off(),
		closeBox(u, r);
	});
}
function showBox(n, t) {
	n.show(),
	t.slideDown(300);
}
function closeBox(n, t) {
	t.slideUp(300,
	function() {
		n.remove(),
		t.remove();
	});
}
function nofind() {
	var n = event.srcElement;
	n.src = $("#hidDefaultImage").val(),
	n.onerror = null;
}

function Init() {
	$("#hidpageindex").val("0");
	//$("#loading").show(),
	//$("#type>li:eq(0)").addClass("xz"),
	$(".shuaxin img").bind("click",
	function() {
		$(".shuaxin img").addClass("shuaxinP"),
		getShoppingListLocation();
	});
	
	InitType(),
	InitPrice(),
	InitDelivery(),
	InitIco(),
	$("#hidpageindex").val("0"),
	bindSupplierDetailEvent()
}
function InitType() {
	$("#type>li").click(function() {
		var n = $(this).attr("id");
		$("#countyid").val(n);
		$("#searchForm").submit();
	});
}
function InitPrice() {
	$("#price>li").click(function() {
		var n = $(this).attr("id");
		$("#cateid").val(n);
		$("#searchForm").submit();
	})
}
function InitDelivery() {
	$("#orderType>li").click(function() {
		var n = $(this).attr("value");
		$("#sort").val(n); 
		$("#searchForm").submit();
	})
}
function InitIco() {
	$(".ico").click(function() {
		location.href = $("#hidSearchSupplierListUrl").val() + "&keyWord="+("#keyWord").val();
	})
}
function loadDataByPage() {
	var n = parseInt($("#hidpageindex").val()) + 1,
	t = $("#type").find(".xz").attr("id"),
	i = $("#hidCityId").val(),
	r = $("#price").find(".xz").val(),
	u = $("#orderType").find(".xz").val(),
	f = $("#hidUserLat").val(),
	e = $("#hidUserLon").val();
	$.ajax({
		url: $("#hidSupplierListUrl").val(),
		global: $("#hidpageindex").val() == 0 ? !0 : !1,
		type: "POST",
		dataType: "html",
		data: {
			keyWord: $("#keyWord").val(),
			type: t,
			price: r,
			areaId: i,
			pageIndex: n,
			orderType: u
		},
		beforeSend: function() {
			n > 1 && ($(".mBxCr").show(), $("#loadtip").addClass("more"), $("#loadtip").text("正在加载..."),$(".nextPage").hide())
		},
		success: function(t) {
			if ($("#supplierlist").find("section").remove(), $("<div>" + t + "</div>").find("li").length == 0){
				$(".mBxCr").hide(),
				$("#hidpageindex").val(n - 1);
			} 
			else {
				$("#hidpageindex").val(n);
				var i = $("#supplierlist").html();
				$("#supplierlist").html(i + t)
			}
			$("#supplierlist").find("li").length == 0 && ($(".mBxCr").hide(), $("#supplierlist").html($("#nocontentdiv").html())),
			bindSupplierDetailEvent()
		},
		complete: function() {
			var r;
			$(".mBxCr").hide();
			$("#loadtip").removeClass("more");
			$("#hidTotalCount").val() == undefined && $("#hidPageSize").val() == undefined && $(".mBxCr").hide();
			var n = 0,
			t = parseInt($("#hidTotalCount").val()),
			i = parseInt($("#hidPageSize").val()),
			u = t % i;
			
			n = u > 0 ? parseInt(t / i) + 1 : parseInt(t / i);
			r = parseInt($("#hidpageindex").val());
			if(n == r || n == null  || isNaN(n)){
				$(".nextPage").hide();
				$(".mBxCr").hide();
			}else{
				$(".nextPage").show();
			}
			$("#loading").hide();
		},
		error: function() {
			console.log("加载搜索餐厅列表数据")
		}
	})
}
function bindSupplierDetailEvent() {
	$("#supplierlist>li").unbind("click");
	$("#supplierlist>li").bind("click",function() {
		var n = $(this).attr("data-id");
		location.href = $("#hidMenu").val() + "/" + n
	}),
	tapcolor(".clkitm")
}
function removeClass() {
	$(".sele").find("b").removeClass("se"),
	$(".sele").find("li").removeClass("se"),
	$(".sub").removeClass("sh"),
	$("#reginlist").find("li").removeClass("xz")
}
function getSupplierList(n, t, i, r) {
	var u = 1;
	$.ajax({
		url: $("#hidSupplierListUrl").val(),
		type: "POST",
		dataType: "html",
		data: {
			keyWord: $("#keyWord").val(),
			type: n,
			price: t,
			areaId: i,
			pageIndex: u,
			orderType: r
		},
		success: function(n) {
			$(".container").show(),
			$("<div>" + n + "</div>").find("li").length == 0 ? ($(".mBxCr").hide(), $("#supplierlist").html($("#nocontentdiv").html())) : $("#supplierlist").html(n),
			bindSupplierDetailEvent();
		},
		error: function() {
			console.log("加载搜索餐厅列表数据");
		}
	});
}
function merchantLink() {
	location.href = $("#hidMerchants").val();
}

function getShoppingListLocation() {
	/**if ($("#address").text("定位中..."), navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("baiduboxapp") > -1 && flag) {
		clouda.device.geolocation.get({
			onsuccess: onSupplierSuccess,
			onfail: onSupplierFail
		});
		return
	}
	if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("HybridETS") > -1) {
		window.geolocation.getCurrentPosition("onSupplierSuccess", "onSupplierFail");
		return
	}
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showShoppingListPosition, showShoppingListError, geooptions);
		return
	}
	$("#address").text("定位失败，无法获取城市");**/
	
//	loadDataByPage();
}
function showShoppingListPosition(n) {
	var t = n.coords.latitude,
	i = n.coords.longitude;
	window.BMap.Convertor.translate(new window.BMap.Point(i, t), 0, getShoppingListLocationCity);
//	var gc = new BMap.Geocoder(); 
//	var pt = new BMap.Point(r.coords.longitude,r.coords.latitude);
//	BMap.Convertor.translate(ggPoint,0,translateCallback);//GCJ-02(GPS)坐标转成百度坐标
	
}
function getShoppingListLocationCity(n){
	var gc = new BMap.Geocoder(); 
	var pt = new BMap.Point(n.lng,n.lat);
	gc.getLocation(pt, function(rs){
		var addComp = rs.addressComponents;
		$.ajax({
			url: $("#hidGetLocationCityUrl").val(),
			type: "POST",
			dataType: "json",
			data: {
				'lat':rs.point.lat,
		    	'lng':rs.point.lng,
		    	'street':addComp.street
			},
			success: function(n) {
				if ($(".shuaxin img").removeClass("shuaxinP"), n.StatusCode != 200) {
					PopUp(n.Message);
					return
				}
				$("#address").text(rs.address),
				getSupplierList($("#type").find(".xz").attr("id"), $("#reginlist").find(".xz").attr("childid"), $("#hidCityId").val(), $("#orderType").find(".xz").val())
			},
			error: function() {
				$(".shuaxin img").removeClass("shuaxinP"),
				$("#address").text("定位失败，无法获取城市")
			}
		});
	}); 
}

function showShoppingListError() {
	$(".shuaxin img").removeClass("shuaxinP");
	$("#address").text("定位失败，无法获取城市");
}






var touchstarts = "ontouchstart" in document ? "touchstart": "mouseover",
tapse = "ontouchstart" in document ? "touchstart": "click",
touchends = "ontouchend" in document ? "touchend": "mouseout",
addclass = function(n, t) {
	$(n).bind("click",
	function() {
		$(this).addClass(t)
	})
},
slidershow = function(n, t) {
	$(n).bind("click",
	function(i) {
		var u = $(i.target);
		if (u.is("div,h3")) {
			$(this).find(t).animate({
				width: 33
			},
			500, "easeOutBounce");
			var f = $(n).index(this),
			e = $(t).eq(f).find(".listshu").html(),
			r = parseInt(e);
			r += 1,
			$(this).find(t + " .listshu").html(r)
		}
	}),
	$$(n).swipeLeft(function() {
		$(this).find(t).animate({
			width: 0
		},
		200)
	})
},
tapcolor = function(n, t) {
	t === undefined && (t = "tapclass"),
	$(n).bind(touchstarts,
	function() {
		$(this).addClass(t).siblings(n).removeClass(t)
	}),
	$(n).bind(touchends,
	function() {
		$(this).removeClass(t)
	})
},
toggleclass = function(n, t) {
	$(n).bind("click",
	function() {
		$(this).toggleClass(t)
	})
},
eventclass = function(n, t, i) {
	$(n).bind(tapse,
	function(r) {
		var u = $(r.target);
		u.is(n) && $(t).toggleClass(i)
	})
},
swval = function(n, t, i, r) {
	$(i).on("click",
	function() {
		var i = $(r).text();
		i === n ? $(r).text(t) : $(r).text(n)
	})
},
mulitab = function(n, t, i) {
	$(t).click(function() {
		var r = $(t).index(this);
		$(t).removeClass(i),
		$(this).addClass(i),
		$(n).hide(),
		$(n).eq(r).show()
	})
},
fBansize = function(n, t) {
	var i = $("body").width(),
	r = i * 7 / 16;
	$(n).height(r),
	$(t).width(i),
	$(t).height(r)
},
flyfun = function(n, t, i) {
	$(n).bind("click",
	function(n) {
		var u = $(n.target);
		if (u.is("div,h3")) {
			$(t).find(".fly").remove(),
			$(this).parent().append('<div class="fly" style="position:absolute;top:40%;left:40%;width:18px;height:18px;border-radius:25px;background:#ea5413; opacity:0.5"></div>');
			var f = $(".fly").offset().top,
			e = $(i).offset().top + 50,
			r = e - f;
			r += "px",
			$(".fly").animate({
				width: "10px",
				height: "10px",
				top: r,
				left: "0px",
				opacity: "0.5"
			},
			function() {
				$(this).parent().find(".fly").remove()
			})
		}
	})
},
counter = function(n, t) {
	$(n).on("click",
	function() {
		var u = '<div class="qipao">0</div>',
		n = $(this).parent().index(),
		f = $(t + " ul li").eq(n).find(".qipao").length,
		e = Boolean(f),
		r,
		i;
		e || $(t + " ul li").eq(n).append(u),
		r = $(t + " ul li").eq(n).find(".qipao").text(),
		i = parseInt(r),
		i += 1,
		$(t + " ul li").eq(n).find(".qipao").text(i),
		$(".qipao").css({
			position: "absolute",
			top: "8px",
			right: "5px",
			width: "16px",
			height: "16px",
			"line-height": "13px",
			"text-align": "center",
			color: "#fff",
			background: "url(images/icos/qipao.png) 0 0 no-repeat",
			backgroundSize: "16px 16px"
		})
	})
},
conHei = function() {
	for (var i = $("html").height(), t = i - 84, n = 0; n < arguments.length; n++) $("" + arguments[n] + "").height(t)
},
conHei2 = function() {
	for (var i = $("html").height(), t = i - 204, n = 0; n < arguments.length; n++) $("" + arguments[n] + "").height(t)
},
toolbar = function(n, t) {
	var i = parseInt($(n).offset().top),
	r = parseInt($(n).offset().left);
	$(window).scroll(function() {
		$(window).scrollTop() > i ? $(n).attr("style", "position:fixed ;left:" + r + "px; top:" + t + "; z-index:99999") : $(n).removeAttr("style")
	})
},
heightUl = function(n) {
	var t = $(window).height() - 54;
	$(n).css({
		height: t + "px"
	})
};
slider = function(n) {
	for (var f = "",
	t = 0,
	e = n,
	r, u, o, i = 0; i < $(".swipe-wrap div").length; i++) f += "<span></span>";
	r = function() {
		return t < $(".swipe-wrap div").length ? t++:t = 0,
		$("#sbsd span").eq(t).trigger("click")
	},
	u = setInterval(r, e),
	$(".swipe-wrap div").hover(function() {
		clearInterval(u)
	},
	function() {
		u = setInterval(r, e)
	}),
	$("#sbsd").html(f),
	$("#sbsd span").eq(0).addClass("on"),
	$("#sbsd span").bind("click",
	function() {
		var n = $("#sbsd span").index(this);
		o.slide(n)
	}),
	o = Swipe(document.getElementById("slider"), {
		continuous: !0,
		callback: function(n) {
			for (var t = $("#sbsd span").length; t--;) $("#sbsd span").removeClass("selects");
			$("#sbsd span").eq(n).addClass("on").siblings("span").removeClass("on")
		}
	})
},
searcha(),
$(function() {
	tapcolor("header div"),
	tapcolor("footer nav ul li")
}),
sliderbottom = function(n, t) {
	$(n).animate({
		bottom: 0
	},
	500),
	$(n).find(t).bind(tapse,
	function() {
		$(n).animate({
			bottom: "-43px"
		},
		500)
	})
},
vostai = function(n, t) {
	$(n).bind(touchstarts,
	function() {
		$(this).addClass(t)
	}),
	$(n).bind(touchends,
	function() {
		$(this).removeClass(t)
	})
},
$(function() {
	tapcolor("header div"),
	tapcolor("footer nav ul li")
}),
$(function() {
	searcher(".searchcloae")
}),
$("#onloading").bind("ajaxSend",
function() {
	$("#loading").show()
}).bind("ajaxComplete",
function() {
	$("#loading").hide()
}),
$(document).ready(function() {
	$(".return").click(function(n) {
		$(this).attr("id") != "noreturn" && (location.href = $("#hidReturnUrl").val()),
		n.stopPropagation()
	})
}),
$(document).ready(function() {
	$("#homeindex").click(function() {
		location.href = $("#hidHomeUrl").val()
	}),
	$("#footerorderlist").click(function() {
		location.href = $("#hidUserUserOrderListUrl").val()
	}),
	$("#favorate").click(function() {
		location.href = $("#hidFavouriteIndexUrl").val()
	}),
	$("#opinion").click(function() {
		location.href = $("#hidFeedbackIndexUrl").val()
	}),
	$("#footerormy").click(function() {
		location.href = $("#hidUserUrl").val()
	}),
	$("#searchForm").submit(function() {
		this.submit();
        //return location.href = $("#hidSupplierSearchUrl").val() + "&keyWord=" + encodeURI($("#keyWord").val()), !1
    })
}),
$(document).ready(function() {
	Init(),
	mulitab(".ri ul", ".le li", "xz"),
	$("#cityname").click(function() {
		location.href = $("#hidShowCituListUrl").val()
	});
	/**$(".nextPage").click(function() {
		loadDataByPage();
	});**/
	$(".sele a").on("click",
	function() {
		$(".container").css("display", "none"),
		$(".sele a").not(this).find(".se").removeClass("se"),
		$(".sele a").not(this).parent().removeClass("se"),
		$(this).parent().toggleClass("se"),
		$(this).find("b").toggleClass("se"),
		$(this).find(".se").length ? $(".sub").addClass("sh") : ($(".sub").removeClass("sh"), $(".container").show());
		var n = $(".sele a").index(this);
		$(".sub>ul").hide(),
		$(".sub>ul").eq(n).show()
	})
}),
geooptions = {
	enableHighAccuracy: !0,
	maximumAge: 5e3,
	timeout: 5e3
},
flag = !1,
onIndexBeginSuccess = function(n) {
	var t = n.longitude,
	i = n.latitude,
	r = new BMap.Point(t, i);
	getIndexBeginLocationCity(r)
},
onIndexBeginFail = function() {},
onIndexSuccess = function(n) {
	var i = n.longitude,
	r = n.latitude,
	t;
	$("#userCity").text("地址解析中..."),
	t = new BMap.Point(i, r),
	getIndexLocationCity(t)
},
onIndexFail = function() {
	$("#userCity").text("定位失败，无法获取城市")
},
onAdressSuccess = function(n) {
	var i = n.longitude,
	r = n.latitude,
	t;
	$("#address").text("地址解析中..."),
	t = new BMap.Point(i, r),
	getSupplierListLocationCity(t)
},
onAdressFail = function() {
	$("#address").text("定位失败，无法获取城市")
},
onSupplierSuccess = function(n) {
/**	var i = n.longitude,
	r = n.latitude,
	t;
	$("#address").text("地址解析中..."),
	t = new BMap.Point(i, r),
	getShoppingListLocationCity(t)**/
},
onSupplierFail = function() {
	$("#address").text("定位失败，无法获取城市")
}